import { Component, OnInit } from '@angular/core';
import { EcobinServiceService } from '../../ecobin-service.service';
import * as socketIo from 'socket.io-client';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  lat : number;
  lng : number;
  lat2 : number;
  lng2 : number;
  lat3 : number;
  lng3 : number;

  binDataArray: Array<any>;
  full = false;
  nearFull = false;
  notFull = true;

  constructor(private ecoService: EcobinServiceService) { }

  ngOnInit() {
    this.loadBin();
    const socket = socketIo('http://localhost:8000');
    socket.on('binData',(data) => this.loadBin());
    this.getBinLocation()

  }

  loadBin() {
    this.ecoService.getBin().subscribe((response) => {
      this.binDataArray = response.data;
      console.log(this.binDataArray);
      
      if(this.binDataArray[0].plasticQty >= 6 || this.binDataArray[0].glassQty >= 10 || this.binDataArray[0].metalQty >= 10){
        this.full = true;
        this.nearFull = false;
        this.notFull = false;
      }else if(this.binDataArray[0].plasticQty >= 4 || this.binDataArray[0].glassQty >= 7 || this.binDataArray[0].metalQty >= 7){
        this.full = false;
        this.nearFull = true;
        this.notFull = false;
      }else{
        this.full = false;
        this.nearFull = false;
        this.notFull = true;
      }
    })
  }

  private getBinLocation() {
   /// locate the bin
   
       this.lat = 13.28131;
       this.lng =  100.923925;
      
       this.lat2 = 13.312870;
       this.lng2 = 100.943576;

 }

 clearBinQty(){
  this.ecoService.resetQty();
  this.ngOnInit()
 }

}
