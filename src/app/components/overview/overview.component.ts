import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js'
import { EcobinServiceService } from '../../ecobin-service.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {
chart:any;
ctx:any;
  constructor(private ecoService: EcobinServiceService) { }

  dataHistory: Array<any>;
  
  glassHis=[0,0,0,0,0,0,0,0,0,0];
  plasHis=[0,0,0,0,0,0,0,0,0,0];
  metalHis=[0,0,0,0,0,0,0,0,0,0];
  yearAll=[];
  year:any
  firstLoad=true
  ngOnInit() {
    this.setDataChart()
  }

  setDataChart(){
    this.ecoService.getHistory().subscribe((response) => {
      this.dataHistory = response.data;
      if(this.firstLoad==true){
        this.year = this.dataHistory[this.dataHistory.length-1]._id.year
      }
      for(let i=0;i<this.dataHistory.length;i++){
        if(this.yearAll.includes(this.dataHistory[i]._id.year) == false){
          this.yearAll.push(this.dataHistory[i]._id.year)
        }
        if(this.dataHistory[i]._id.year == this.year){
          console.log(this.dataHistory[i]);
          console.log(this.dataHistory[i]._id.month);
          for(let j=1;j<=9;j++){
            var numStr = 0+j.toString();
            if(this.dataHistory[i]._id.month == numStr){
              this.glassHis[j-1] = this.dataHistory[i].glass
              this.plasHis[j-1] = this.dataHistory[i].plastic
              this.metalHis[j-1] = this.dataHistory[i].metal
            }
          }
          for(let l=10;l<=12;l++){
            if(this.dataHistory[i]._id.month == l.toString()){
              this.glassHis[l-1] = this.dataHistory[i].glass
              this.plasHis[l-1] = this.dataHistory[i].plastic
              this.metalHis[l-1] = this.dataHistory[i].metal
            }
          }
     
        }
      }
      this.createChart()
    })
  }

  selecYear(item){
    console.log(item);
    this.year = item;
    this.glassHis=[0,0,0,0,0,0,0,0,0,0];
    this.plasHis=[0,0,0,0,0,0,0,0,0,0];
    this.metalHis=[0,0,0,0,0,0,0,0,0,0];
    this.yearAll=[];
    this.firstLoad = false
    this.ngOnInit();
    this.chart.destroy();
    // this.ecoService.getHistory().subscribe((response) => {
    //   this.dataHistory = response.data;
    //   for(let i=0;i<this.dataHistory.length;i++){
    //     if(this.yearAll.includes(this.dataHistory[i]._id.year) == false){
    //       this.yearAll.push(this.dataHistory[i]._id.year)
    //     }
    //     if(this.dataHistory[i]._id.year == this.year){
    //       console.log(this.dataHistory[i]);
    //       console.log(this.dataHistory[i]._id.month);
    //       for(let j=1;j<=9;j++){
    //         var numStr = 0+j.toString();
    //         if(this.dataHistory[i]._id.month == numStr){
    //           this.glassHis[j-1] = this.dataHistory[i].glass
    //           this.plasHis[j-1] = this.dataHistory[i].plastic
    //           this.metalHis[j-1] = this.dataHistory[i].metal
    //         }
    //       }
    //       for(let l=10;l<=12;l++){
    //         if(this.dataHistory[i]._id.month == l.toString()){
    //           this.glassHis[l-1] = this.dataHistory[i].glass
    //           this.plasHis[l-1] = this.dataHistory[i].plastic
    //           this.metalHis[l-1] = this.dataHistory[i].metal
    //         }
    //       }
     
    //     }
    //   }
    //   this.createChart()
    // })
  }

createChart(){
  this.ctx = document.getElementById('myChart');
  this.chart = new Chart(this.ctx,{
    type:'bar',
    data:{
      labels: ["มกราคม ","กุมภาพันธ์ ","มีนาคม ","เมษายน ","พฤษภาคม ","มิถุนายน ","กรกฎาคม ","สิงหาคม ","กันยายน ","ตุลาคม ","พฤศจิกายน ","ธันวาคม "],
      datasets: [{
        label: "Plastic" ,
        backgroundColor:[
        
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
          'rgb(0,191,255)',
        ],
        borderColor :'rgb(0,0,0)',
        // data:[25,12,54,44,43,48,60,71,50,50,89,54,22]
        data:this.plasHis,
      },{
        label: "Glass" ,
        backgroundColor:[
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
          'rgb(124,252,0)',
        ],
        borderColor :'rgb(0,0,0)',
        // data:[80,20,21,50,78,46,105,56,100,123,50,87,16]
        data:this.glassHis,
      },{
        label: "Metal" ,
        backgroundColor:[
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          'rgb(255,140,0)',
          
        ],
        borderColor :'rgb(0,0,0)',
        // data:[52,89,74,22,62,85,49,80,70,99,55,90,80]
        data:this.metalHis,
      }]
    },
    options:{}
  });
}

}


