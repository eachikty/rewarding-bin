import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EcobinServiceService {

  constructor(private http:Http) { }

  getBin(){
    return this.http.get("http://localhost:8000/bin").pipe(map((res)=>res.json()));
  }

  getHistory(){
    return this.http.get("http://localhost:8000/historyAll").pipe(map((res)=>res.json()));
  }

  resetQty(){
    return this.http.put("http://localhost:8000/bin/resetQty",'').toPromise();
  }
}
