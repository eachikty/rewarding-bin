import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { HomeComponent } from './components/home/home.component';
import { OverviewComponent } from './components/overview/overview.component'
import { EcobinServiceService } from './ecobin-service.service';
import { AgmCoreModule } from '@agm/core';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OverviewComponent,
  ],
  imports: [
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyAR0c98c9a4NyzwGlHJUT_t5YbMBBCC4CQ '
    }),
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'overview',
        component: OverviewComponent
      }
    ])
  ],
  providers: [EcobinServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
