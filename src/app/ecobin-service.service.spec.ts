import { TestBed, inject } from '@angular/core/testing';

import { EcobinServiceService } from './ecobin-service.service';

describe('EcobinServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EcobinServiceService]
    });
  });

  it('should be created', inject([EcobinServiceService], (service: EcobinServiceService) => {
    expect(service).toBeTruthy();
  }));
});
